FROM ubi8/python-39 as Zero

ENV PATH="/opt/app-root/src/miniconda3/bin:${PATH}"
ARG PATH="/opt/app-root/src/miniconda3/bin:${PATH}"

USER 0

RUN yum repolist

RUN yum install  --disableplugin=subscription-manager -y wget && rm -rf /var/lib/apt/lists/*

RUN wget \
    https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh \
    && mkdir /root/.conda \
    && bash Miniconda3-latest-Linux-x86_64.sh -b \
    && rm -f Miniconda3-latest-Linux-x86_64.sh 

RUN conda --version

FROM Zero as One   

# Environement is loaded from 
# conda env export > environment.yaml

COPY app/environment.yaml /app/environment.yaml

WORKDIR /app
RUN conda env create -n dev -f environment.yaml
RUN conda list

# Adding conda pack to reduce image size
RUN conda install -c conda-forge conda-pack

# Packing env and moving to venv
RUN conda-pack -n dev -o /tmp/env.tar && \
  mkdir /venv && cd /venv && tar xf /tmp/env.tar && \
  rm /tmp/env.tar

# We've put venv in same path it'll be in final image,
# so now fix up paths:
RUN /venv/bin/conda-unpack

FROM ubi8-minimal AS runtime

# Copy /venv and /app from the previous stage:

COPY --from=One /venv /venv

# When image is run, run the code with the environment
# activated:

RUN source /venv/bin/activate

# Launching the app
COPY ./app /app
WORKDIR /app
ENTRYPOINT ["/venv/bin/python", "app.py"]
# CMD ["app.py"]
