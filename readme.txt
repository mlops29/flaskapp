** Simple flask App
** Build with conda but runtime without to decrease image size

The multi-stage build process uses diffrent base image to reduce size.
Conda-pack helps to compact the conda environement.



First we use ubi8-python and install conda.
The conda environement is loaded and the requirements installed from environement.yaml

Once we have all required packages in the conda env we use conda-pack to tar it and promote to the runtime image

----

Export app.env like this :

```
conda env export > environment.yaml
```
